import { shallowMount } from '@vue/test-utils'
import Calculadora from '@/components/Calculadora.vue'

describe('Calculadora.vue', () => {
    const wrapper = shallowMount(Calculadora)

    test('Validar que el arranque de la calculadora siempre este vacio', () => {
        expect(wrapper.vm.$data.a).toBe('')
        expect(wrapper.vm.$data.b).toBe('')
     
    });

    test('Suma de dos números de forma satisfactoria', () => {
        //Arrange
        wrapper.vm.$data.a = 3
        wrapper.vm.$data.b = 2
        //Act
        wrapper.vm.calcular(wrapper.vm.$data.a,wrapper.vm.$data.b)
        //Assert
        expect(wrapper.vm.$data.result).toBe(5)
    });

    test('Suma de dos números incluso si son string', () => {
        wrapper.vm.$data.a = 4
        wrapper.vm.$data.b = '2'
        wrapper.vm.calcular(wrapper.vm.$data.a,wrapper.vm.$data.b)
        expect(wrapper.vm.$data.result).toBe(6)
    });

    test('Error al enviar parámetros vacios', () => {
        wrapper.vm.$data.a = 4
        wrapper.vm.$data.b = 1
        wrapper.vm.calcular(wrapper.vm.$data.a,wrapper.vm.$data.b)
        expect(wrapper.vm.$data.result).toBeNaN()
    });
});


